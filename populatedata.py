from models import engine, db_session, Base, Employee, Department
Base.metadata.create_all(bind=engine)

# Fill the tables with some data
 
engineering = Department(name='Engineering', salary=100)
db_session.add(engineering)
hr = Department(name='Human Resources', salary=200)
db_session.add(hr)
it = Department(name='Information Technology', salary=300)
db_session.add(it)
service = Department(name='Customer Service', salary=400)
db_session.add(service)

roy = Employee(name='Roy', department=engineering)
db_session.add(roy)
john = Employee(name='John', department=hr)
db_session.add(john)
alex = Employee(name='Alex', department=it)
db_session.add(alex)
brian = Employee(name='Brian', department=service)
db_session.add(brian)


db_session.commit()