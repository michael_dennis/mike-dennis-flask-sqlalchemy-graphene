# flask_sqlalchemy/schema.py
import graphene
import math
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from models import db_session, Department as DepartmentModel, Employee as EmployeeModel
from sqlalchemy import and_, func
 
class Department(SQLAlchemyObjectType):
    class Meta:
        model = DepartmentModel
        interfaces = (relay.Node, )

class Employee(SQLAlchemyObjectType):
    class Meta:
        model = EmployeeModel
        interfaces = (relay.Node, )

class PageDetails(graphene.ObjectType):
    totalEmployees = graphene.Int()
    totalPages = graphene.Int()
    currentPage = graphene.Int()

class EmployeePage(graphene.ObjectType):
    pageDetails = graphene.Field(PageDetails)
    employees = graphene.List(Employee)

class DepartmentConnection(relay.Connection):
    class Meta:
        node = Department

class EmployeeConnections(relay.Connection):
    class Meta:
        node = Employee

class Query(graphene.ObjectType):

    node = relay.Node.Field()
    # Allows sorting over multiple columns, by default over the primary key
    all_employees = SQLAlchemyConnectionField(Employee) 
    # Disable sorting over this field
    all_departments = SQLAlchemyConnectionField(DepartmentConnection, sort=None)

    employee_page = graphene.Field(EmployeePage, offset = graphene.Int(), limit = graphene.Int(), sortField = graphene.String())

    def resolve_employee_page(self,info, **args):
        offset = args.pop('offset')
        limit = args.pop('limit')
        sortField = args.pop('sortField')
        query = Employee.get_query(info)
        count = db_session.query(func.count(EmployeeModel.id)).scalar()
        pages, currentPage = 0, 0
        pageDetails = PageDetails()
        if count > 0:
            
            if offset < 0:
                offset = 0

            if limit < 1:
                limit = 1
            elif limit > count:
                limit = count

            pages = math.ceil(count / limit)

            if offset >= count:
                offset = pages * limit - limit

            firstRecord = offset + 1

            if offset > 0:
                currentPage = math.ceil(firstRecord / limit)
            else:
                currentPage = 1

            if count - offset < limit:
                offset = (pages - 1) * limit - 1
                print('adjusting offset to {0} for full page'.format(offset))

        employeePage = EmployeePage()
        employeePage.pageDetails = PageDetails(totalEmployees=count,totalPages=pages,currentPage=currentPage)

        if sortField.upper() == 'DEPARTMENT':
            sortFieldModel = DepartmentModel.name
        elif sortField.upper() == 'SALARY':
            sortFieldModel = DepartmentModel.salary

        employees = query.join(DepartmentModel, EmployeeModel.department_id == DepartmentModel.id).order_by(sortFieldModel.desc()).offset(offset).limit(limit).all()


        employeePage.employees = employees
        return employeePage


class createEmployee(graphene.Mutation):
    class Arguments:
        name = graphene.String()
        departmentName = graphene.String()

    ok = graphene.Boolean()
    employee = graphene.Field(Employee)

    def mutate(self, info, name, departmentName):
        query = Department.get_query(info)

        department = query.filter(DepartmentModel.name == departmentName).first()

        employee = EmployeeModel(name=name, department=department)
        db_session.add(employee)
        db_session.commit()
        ok = True
        return createEmployee(employee=employee,ok=ok)

class MyMutations(graphene.ObjectType):
    createEmployee = createEmployee.Field()

schema = graphene.Schema(query=Query, mutation=MyMutations, types=[Employee])