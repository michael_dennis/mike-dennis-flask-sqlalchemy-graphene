from app import app, schema, Department
import unittest
import graphene
from models import db_session, Department as DepartmentModel, Employee as EmployeeModel
from graphql_relay.node.node import to_global_id
from sqlalchemy import func

 
class FlaskTestCase(unittest.TestCase):

	def test_departments_exist(self):
		print('*****************************************************')
		print('Testing If Department Records Exist')
		count = db_session.query(func.count(DepartmentModel.id)).scalar()
		print('Total Departments: {}'.format(count))
		self.assertTrue(count > 0)

	def test_employees_exist(self):
		print('*****************************************************')
		print('Testing If Employee Records Exist')
		count = db_session.query(func.count(EmployeeModel.id)).scalar()
		print('Total Employees: {}'.format(count))
		self.assertTrue(count > 0)


	def test_mutation_create_employee(self):
		print('*****************************************************')
		print('Testing createEmployee mutation')
		initialCount = db_session.query(func.count(EmployeeModel.id)).scalar()
		print('Employee count before mutation - createEmployee: {}'.format(initialCount))
		testEmployeeName = "testuser123"
		query = ''' 
		mutation {
  			createEmployee(name: "%s", departmentName: "Engineering") {
    			employee {
    				id
    			}
 			 }
		} '''
		print("executing query:")
		print (query % testEmployeeName)
		result = schema.execute(query % testEmployeeName)

		count = db_session.query(func.count(EmployeeModel.id)).scalar()

		self.assertTrue(count > initialCount)

		print('Employee count after mutation - createEmployee: {}'.format(count))

		print('Cleaning up Employee: {}'.format(testEmployeeName))

		testEmployee = db_session.query(EmployeeModel).filter(EmployeeModel.name == testEmployeeName).first()

		db_session.delete(testEmployee)
		db_session.commit()
		count = db_session.query(func.count(EmployeeModel.id)).scalar()
		print('Post Cleanup Employee count: {}'.format(count))
		self.assertTrue(count == initialCount)


	def test_get_all_employees_one_page(self):
		print('*****************************************************')
		print('Testing all employees on one page')
		count = db_session.query(func.count(EmployeeModel.id)).scalar()
		query = ''' 
		{
	  	employeePage(offset: 0, limit: %s, sortField: "department") {
		    pageDetails {
		      totalEmployees
		      totalPages
		      currentPage
		    }
		    employees {
		      id
		    }
		  }
		} '''

		print("executing query:")
		print (query % count)
		result = schema.execute(query % count)	

		pageDetailsTotalEmployees = result.data['employeePage']['pageDetails']['totalEmployees']
		employeeRecordOnPageCount = len(result.data['employeePage']['employees'])
		totalPageCount = result.data['employeePage']['pageDetails']['totalPages']

		print('Total Records In Database: {} - Total Employees On Page {} - Total Pages {}'.format(count,employeeRecordOnPageCount, totalPageCount))

		self.assertTrue(pageDetailsTotalEmployees == employeeRecordOnPageCount)
		self.assertTrue(employeeRecordOnPageCount == count)

	def test_sort_department(self):
		print('*****************************************************')
		print("Testing sort by department name")
		departmentA = DepartmentModel(name='aaaa', salary=2000)
		db_session.add(departmentA)
		departmentB = DepartmentModel(name='aaab', salary=1000)
		db_session.add(departmentB)
		deplartmentAEmployee = EmployeeModel(name='aaaa', department=departmentA)
		db_session.add(deplartmentAEmployee)
		deplartmentBEmployee = EmployeeModel(name='aaab', department=departmentB)
		db_session.add(deplartmentBEmployee)
		db_session.commit()

		query = ''' 
		{
	  	employeePage(offset: 0, limit: 2, sortField: "department") {
		    employees {
		      name
		      department{
		      	name
		      	salary
		      }
		    }
		  }
		} '''

		print("executing query:")
		print (query)
		result = schema.execute(query)	

		firstResultValue = result.data['employeePage']['employees'][0]['department']['name']
		secondResultValue = result.data['employeePage']['employees'][1]['department']['name']

		print(result.data)
		print('First Result Department: {}'.format(firstResultValue))
		print('Second Result Department: {}'.format(secondResultValue))

		db_session.delete(deplartmentAEmployee)
		db_session.delete(deplartmentBEmployee)
		db_session.delete(departmentA)
		db_session.delete(departmentB)
		db_session.commit()
		self.assertTrue(firstResultValue > secondResultValue)

	def test_sort_salary(self):
		print('*****************************************************')
		print('Testing sort by department salary')
		departmentA = DepartmentModel(name='aaaa', salary=2000)
		db_session.add(departmentA)
		departmentB = DepartmentModel(name='aaab', salary=1000)
		db_session.add(departmentB)
		deplartmentAEmployee = EmployeeModel(name='aaaa', department=departmentA)
		db_session.add(deplartmentAEmployee)
		deplartmentBEmployee = EmployeeModel(name='aaab', department=departmentB)
		db_session.add(deplartmentBEmployee)
		db_session.commit()

		query = ''' 
		{
	  	employeePage(offset: 0, limit: 2, sortField: "salary") {
		    employees {
		      name
		      department{
		      	name
		      	salary
		      }
		    }
		  }
		} '''

		print("executing query:")
		print (query)
		result = schema.execute(query)	

		firstResultValue = result.data['employeePage']['employees'][0]['department']['salary']
		secondResultValue = result.data['employeePage']['employees'][1]['department']['salary']

		print(result.data)
		print('First Result Salary: {}'.format(firstResultValue))
		print('Second Result Salary: {}'.format(secondResultValue))

		db_session.delete(deplartmentAEmployee)
		db_session.delete(deplartmentBEmployee)
		db_session.delete(departmentA)
		db_session.delete(departmentB)
		db_session.commit()

		self.assertTrue(firstResultValue > secondResultValue)

if __name__ == '__main__':
	unittest.main()