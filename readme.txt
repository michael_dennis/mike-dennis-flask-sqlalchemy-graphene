﻿******************** Setup ********************

1)	Navigate to root directory in terminal and run the following:

		virtualenv env
		IF Python2
			source env/bin/activate  # On Windows use `env\Scripts\activate`

		IF Python3
			env/Source/activate.bat
 

		# SQLAlchemy and Graphene with SQLAlchemy support
		pip install SQLAlchemy
		pip install graphene_sqlalchemy

		# Install Flask and GraphQL Flask for exposing the schema through HTTP
		pip install Flask
		pip install Flask-GraphQL

2)	Setup the sqlite database
		
		python ./populatedata.py

3) Run Unit Tests

		python ./tests.py

4)	Start the Flask server
		
		python ./app.py

5)	http://127.0.0.1:5000/graphql for manual queries

******************** Manual Queries ********************


Get Employee Page (sort by "department" or "salary")

{
  employeePage(offset: 0, limit: 2, sortField: "department") {
    pageDetails {
      totalEmployees
      totalPages
      currentPage
    }
    employees {
      id
      name
      	department{
          name
          salary
        }
    }
  }
}


Create Employee

mutation {
  createEmployee(name: "testuser", departmentName: "Engineering") {
    ok
    employee {
      name
      id
    }
  }
}

